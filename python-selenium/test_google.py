from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest
import os
import sys

class YandexSearch(unittest.TestCase):

    global search

    def setUp(self):
        self.search = 'CHeGoViCHoG'
        self.driver = webdriver.Chrome(executable_path = os.path.abspath("include/Drivers Browser/chromedriver"))
        # connect chrome driver \\ подключаем драйвер хрома
        self.driver.get('https://yandex.ru') # Entrance to Yandex \\ входв яндекс


    def test_01(self):
        driver = self.driver
        input_field = driver.find_element_by_xpath('//*[@id="text"]') # Find to entry field \\ находим поле ввода

        input_field.send_keys(self.search)  # find to word \\ найти по слову
        input_field.send_keys(Keys.ENTER)  # press enter \\ нажать ввод

        time.sleep(2) # sleep \\ сон

        titles = driver.find_elements_by_class_name('serp-item')
        for title in titles:
            assert self.search in title.text.lower()


    def test_02(self):
        driver = self.driver
        assert 'Яндекс' in driver.page_source
        input_field = driver.find_element_by_xpath('//*[@id="text"]')
        input_field.send_keys(self.search)

        time.sleep(1)

        input_field.send_keys(Keys.DOWN)
        input_field.send_keys(Keys.ENTER)
        assert "No results found." not in driver.page_source

        time.sleep(3)

        titles = driver.find_elements_by_class_name('a11y-hidden')
        for title in titles:
            assert self.search in title.text.lower()

    def tearDown(self):
        self.driver.close()

if __name__ == '__main__':
        unittest.main()
